import yaml
import sys

DOCKER_COMPOSE_VERSION = "3.7"

def correct_version(compose):
    print("-> compose file version is correct")
    if compose["version"] != DOCKER_COMPOSE_VERSION:
        raise Exception(f"wrong docker-compose version: found {compose['version']} but should be {DOCKER_COMPOSE_VERSION}")

def uses_hashed_images(compose):
    print("-> service images are pinned")
    for service in compose["services"]:
        if not "image" in compose["services"][service]:
            continue
        image = compose["services"][service]["image"]
        split = image.split("@")
        if len(split) != 2 or not split[1].startswith("sha256"):
            raise Exception(f"image {split[0]} is not pinned with a hash")

def not_privileged_services(compose):
    print("-> no privileged services")
    for service in compose["services"]:
        if "privileged" in compose["services"][service]:
            if compose["servics"][service]["privileged"] != False:
                raise Exception(f"service {service} is privileged")

def has_expose_var(compose):
    print("-> has EXPOSE variable")
    if not "EXPOSE" in compose["services"]["link"]["environment"]:
        raise Exception(f"EXPOSE variable missing")

if __name__ == "__main__":
    app = sys.argv[1]
    print(f"=> checking {app}")
    compose = yaml.safe_load(open(f"{app}/docker-compose.yml"))
    print("-> loaded docker-compose")
    try:
        correct_version(compose)
        uses_hashed_images(compose)
        not_privileged_services(compose)
        has_expose_var(compose)
    except Exception as e:
        print(f"Error: {e}")
        exit(1)
