#!/bin/bash

# fail if app name was not provided
if [[ $# -ne 1 ]]; then
    echo "No app specified"
    exit 1
fi

# ensure provided app is all lowercase
APP="${1,,}"

# adds underscores in case the app name is smaller than 4 characters
APP_NAME_EXTENDED="${APP}______"

# get first 2 characters of app
FIRST_DIRECTORY=${APP_NAME_EXTENDED:0:2}
# get second 2 characters of app
SECOND_DIRECTORY=${APP_NAME_EXTENDED:2:2}

APP_DIRECTORY="$FIRST_DIRECTORY/$SECOND_DIRECTORY/$APP"
mkdir -p $APP_DIRECTORY

# create docker-compose and metadata files for app
touch $APP_DIRECTORY/docker-compose.yml $APP_DIRECTORY/manifest.yml
